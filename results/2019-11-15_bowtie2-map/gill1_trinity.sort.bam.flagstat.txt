503184688 + 0 in total (QC-passed reads + QC-failed reads)
0 + 0 secondary
0 + 0 supplementary
0 + 0 duplicates
468491256 + 0 mapped (93.11% : N/A)
503184688 + 0 paired in sequencing
251592344 + 0 read1
251592344 + 0 read2
418247400 + 0 properly paired (83.12% : N/A)
449003590 + 0 with itself and mate mapped
19487666 + 0 singletons (3.87% : N/A)
27400582 + 0 with mate mapped to a different chr
9911012 + 0 with mate mapped to a different chr (mapQ>=5)
