#!/bin/bash
#SBATCH --ntasks-per-node=1
#SBATCH -D .
#SBATCH -c 1
#SBATCH --job-name transabyss
#SBATCH --mem-per-cpu=5781
#SBATCH --mail-user=petersen@ie-freiburg.mpg.de
#SBATCH --mail-type=ALL
#SBATCH -p bioinfo
#SBATCH -o %x.%j.out
#SBATCH -e %x.%j.err

set -e
set -o nounset
set -o pipefail

export LANGUAGE='en_US.UTF-8'
export LC_ALL='en_US.UTF-8'

if [[ $# -ne 3 ]]; then
	echo "Usage: $0 forward reverse output_dir"
	exit 1
fi

forward=$1
reverse=$2
output_dir=$3

module load transabyss

mkdir -p $output_dir

for kmer in 25 35 45 55; do
	name=k${kmer}
	assemblydir=$output_dir/${name}
	mkdir -p $assemblydir
	transabyss -k ${kmer} --pe $forward $reverse --SS --outdir ${assemblydir} --name ${name} --threads $SLURM_CPUS_PER_TASK
done

mergedassembly=$output_dir/merged.fa
transabyss-merge --threads $SLURM_CPUS_PER_TASK --mink 25 --maxk 55 --SS --prefixes k{25,35,45,55}. --out ${mergedassembly} $output_dir/k{25,35,45,55}/*-final.fa 
