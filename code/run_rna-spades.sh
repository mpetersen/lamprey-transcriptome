#!/bin/bash
#SBATCH --ntasks-per-node=1
#SBATCH -D .
#SBATCH --cpus-per-task 4
#SBATCH --job-name rnaspades-%j
#SBATCH --mem-per-cpu=5781
#SBATCH --mail-user=petersen@ie-freiburg.mpg.de
#SBATCH --mail-type=ALL
#SBATCH -p bioinfo
#SBATCH -o %x.%j.out
#SBATCH -e %x.%j.err

set -e
set -o nounset
set -o pipefail

export LANGUAGE='en_US.UTF-8'
export LC_ALL='en_US.UTF-8'

if [[ $# -ne 3 ]]; then
	echo "Usage: $0 left right output_dir"
	exit 1
fi

left=$1
right=$2
output_dir=$3

module load spades

echo "## $(date -Is) Running rna-SPAdes"

mkdir -p $output_dir
rnaspades.py --threads $SLURM_CPUS_PER_TASK --pe1-1 $left --pe1-2 $right --pe1-fr -o $output_dir --cov-cutoff auto

echo "## $(date -Is) Done"
