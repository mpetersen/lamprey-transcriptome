import os
import pandas as pd
from snakemake.utils import validate, min_version
min_version("5.1.2")

configfile: "config.yaml"
validate(config, schema = "schemas/config.schema.yaml")

samples = pd.read_csv(config["samples"], sep = '\t').set_index("sample", drop=False)
assemblers = pd.read_csv(config["assemblers"], sep = '\t').set_index("assembler", drop=False)

localrules: link_assembly

rule all:
	input: "reports/assembly/summary.tsv"

def get_assembly_fasta(wildcards):
	assembly_fasta = assemblers.loc[ wildcards.assembler, "assembly_fasta"]
	path = "results/assembly/{sample}/{assembler}/{assembly_fasta}".format(sample = wildcards.sample, assembler= wildcards.assembler, assembly_fasta = assembly_fasta)
	return(path)

rule link_assembly:
	input:	
		get_assembly_fasta
	output:
		"results/assembly/{sample}/{assembler}/{sample}.{assembler}.fa"
	shell:
		"""
		ln -rs {input} {output}
		"""

rule kallisto_index:
	input: "results/assembly/{sample}/{assembler}/{sample}.{assembler}.fa"
	output: "results/assembly/{sample}/qc/{assembler}/kallisto/{sample}_{assembler}.kallisto.idx"
	shell:
		"""
		module load kallisto
		kallisto index --index {output} {input}
		"""

rule kallisto_quant:
	input: 
		index = "results/assembly/{sample}/qc/{assembler}/kallisto/{sample}_{assembler}.kallisto.idx",
		reads = [ "data/clean/{sample}_R1.fastq.gz", "data/clean/{sample}_R2.fastq.gz" ]
	output:	"results/assembly/{sample}/qc/{assembler}/kallisto/abundance.tsv"
	params:
		outdir = "results/assembly/{sample}/qc/{assembler}/kallisto"
	threads: 8
	shell:
		"""
		module load kallisto
		kallisto quant --threads {threads} --bootstrap-samples 100 --index {input.index} --output-dir {params.outdir} {input.reads}
		echo '## {rule} done'
		"""

rule link_kallisto_tsv:
	input: "results/assembly/{sample}/qc/{assembler}/kallisto/abundance.tsv"
	output: "results/assembly/{sample}/qc/{assembler}/expression/abundance_{sample}_{assembler}.tsv"
	shell: "ln -rs {input} {output}"

rule expression_matrix:
	input: "results/assembly/{sample}/qc/{assembler}/expression/abundance_{sample}_{assembler}.tsv"
	output: "results/assembly/{sample}/qc/{assembler}/expression/{sample}.isoform.TPM.not_cross_norm"
	params:
		outdir = "results/assembly/{sample}/qc/{assembler}/expression"
	shell:
		"""
		module load Trinity
		abundance_estimates_to_matrix.pl --est_method kallisto --gene_trans_map none --out_prefix {params.outdir}/{wildcards.sample} {input}
		"""

rule count_expressed_transcripts:
	input: "results/assembly/{sample}/qc/{assembler}/expression/{sample}.isoform.TPM.not_cross_norm"
	output: "results/assembly/{sample}/qc/{assembler}/expression/{sample}.genes_matrix.TPM.not_cross_norm.counts_by_min_TPM"
	shell:
		"""
		module load Trinity
		count_matrix_features_given_MIN_TPM_threshold.pl {input} > {output}
		"""

rule plot_neg_min_tpm:
	input: "results/assembly/{sample}/qc/{assembler}/expression/{sample}.genes_matrix.TPM.not_cross_norm.counts_by_min_TPM"
	output: "results/assembly/{sample}/qc/{assembler}/expression/{sample}.genes_matrix.TPM.not_cross_norm.counts_by_min_TPM.plot.pdf"
	shell: "python3 code/plot_counts_by_min_TPM.py {input} {output}"

		

rule count_transcripts_over_threshold:
	input: "results/assembly/{sample}/qc/{assembler}/kallisto/abundance.tsv"
	output: "reports/assembly/{sample}/{assembler}.expressed.tsv"
	shell:
		"""
		printf "%s\t%s\t%s\t" "{wildcards.sample}" "{wildcards.assembler}" "expressed_transcripts" > {output}
		awk '$5 > 2 {{ c++ }} END {{ print c }}' {input} >> {output}
		"""

rule summarise:
	input: expand("reports/assembly/{sample}/{assembler}.expressed.tsv", sample = samples["sample"], assembler = assemblers["assembler"])
	output: "reports/assembly/summary.tsv"
	shell: "cat {input} > {output}"

def get_fastq(wildcards):
	 return(samples.loc[ wildcards.sample, ["read1", "read2"]].dropna())

rule rna_spades:
	input: get_fastq
	output:
		"results/assembly/{sample}/rna-spades/transcripts.fasta"
	threads: 48
