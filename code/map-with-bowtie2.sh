#!/bin/bash
#SBATCH --ntasks-per-node=1
##SBATCH -D /data/processing/petersen/lamprey-transcriptome
#SBATCH -c 4
#SBATCH --job-name test
#SBATCH --mem-per-cpu=5781
#SBATCH --mail-user=petersen@ie-freiburg.mpg.de
#SBATCH --mail-type=ALL
#SBATCH -p bioinfo
#SBATCH -o %x.%j.out
#SBATCH -e %x.%j.err

set -e
set -o nounset
set -o pipefail

export LANGUAGE='en_US.UTF-8'
export LC_ALL='en_US.UTF-8'

if [[ $# -ne 4 ]]; then
	echo "Usage: $0 reference_index forward_reads reverse_reads output_bamfile"
	exit 1
fi

reference=$1
forward=$2
reverse=$3
outfile=$4

module load bowtie2
module load samtools

mkdir -p $(dirname $outfile)

if [[ ! -f "$reference".1.bt2 ]]; then
	echo '## No bowtie2 index found, building index'
	bowtie2-build $reference $reference
fi
echo '## Mapping reads with bowtie2'
bowtie2 \
	--threads $SLURM_CPUS_PER_TASK \
	-x $reference \
	-1 $forward \
	-2 $reverse \
	| samtools sort -@ $SLURM_CPUS_PER_TASK - \
	| samtools view -h -b \
	> $outfile

echo '## Getting mapping statistics with samtools flagstat'
samtools flagstat ${outfile} > ${outfile}.flagstat.txt

echo '## Done'
