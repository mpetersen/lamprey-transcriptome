import matplotlib.pyplot as plt
import pandas as pd
import sys

data = pd.read_csv(sys.argv[1], sep = '\t')
f = plt.figure()
plt.plot(data["neg_min_tpm"], data["num_features"], "o")
plt.ylabel("num_features")
plt.xlabel("neg_min_tpm")
f.savefig(sys.argv[2])

