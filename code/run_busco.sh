#!/bin/bash
#SBATCH --ntasks-per-node=1
#SBATCH -D /data/processing/petersen/lamprey-transcriptome
#SBATCH -c 4
#SBATCH --job-name busco
#SBATCH --mem-per-cpu=5781
#SBATCH --mail-user=petersen@ie-freiburg.mpg.de
#SBATCH --mail-type=ALL
#SBATCH -p bioinfo
#SBATCH -o %x.%j.out
#SBATCH -e %x.%j.err

set -e
set -o nounset
set -o pipefail

export LANGUAGE='en_US.UTF-8'
export LC_ALL='en_US.UTF-8'

if [[ $# -ne 3 ]]; then
	echo "Usage: $0 busco_dir assembly_fasta output_dir"
	exit 1
fi

busco_dir=$1
assembly_fasta=$2
output_dir=$3
assembly_fasta_basename=$(basename $assembly_fasta)

module load busco

echo '## Running BUSCO analysis'

mkdir -p $output_dir
ln -frs $assembly_fasta $output_dir

basedir=$(pwd)
cd $output_dir

run_busco --cpu $SLURM_CPUS_PER_TASK -i $assembly_fasta_basename -l $basedir/data/raw/busco/vertebrata_odb9 --mode transcriptome --species human,eukaryota --evalue 1e-5 --out "busco_$assembly_fasta_basename" --tmp_path tmp_$assembly_fasta_basename

cd $basedir
echo '## Done'
